package com.wolfpack.rendezvous.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.wolfpack.rendezvous.data.RendezvousRepository
import com.wolfpack.rendezvous.models.Rendezvous

open class RendezvousViewModel(application: Application): AndroidViewModel(application) {
    private val rendezvousRepository : RendezvousRepository  = RendezvousRepository(application)

    fun createRendezvous(rendezvous: Rendezvous) = rendezvousRepository.createRendezvous(rendezvous)

    fun getAllRendezvous() = rendezvousRepository.getAllRendezvous()
}