package com.wolfpack.rendezvous

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.wolfpack.rendezvous.models.Rendezvous
import com.wolfpack.rendezvous.viewmodels.RendezvousViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), BaseRendezvousDialog.RendezvousDialogListener {
    override fun onCreateRendezvous(rendezvous: Rendezvous) {
        viewModel.createRendezvous(rendezvous)
    }

    override fun onUpdateRendezvous(rendezvous: Rendezvous) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private val viewModel: RendezvousViewModel by lazy {
        ViewModelProviders.of(this).get(RendezvousViewModel::class.java)
    }

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        fab.setOnClickListener { _ ->
            val dialog = CreateDialogFragment()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            transaction
                    .add(android.R.id.content, dialog)
                    .addToBackStack(null)
                    .commit()
        }


        viewManager = LinearLayoutManager(this)
        viewAdapter = RendezvousAdapter()

        val itemDecorator = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider)!!)

        recyclerView = findViewById<RecyclerView>(R.id.content).apply {
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter
            addItemDecoration(itemDecorator)
        }

        viewModel.getAllRendezvous().observe(this,  Observer {
            (viewAdapter as RendezvousAdapter).setRendezvous(it!!)
        })

        setSupportActionBar(toolbar)
        mSectionsPagerAdapter = SectionsPagerAdapter()

        pager.adapter = mSectionsPagerAdapter
        tab_layout.setupWithViewPager(pager, true)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    inner class SectionsPagerAdapter : PagerAdapter() {
        override fun isViewFromObject(p0: View, p1: Any): Boolean {
            return p1 == p0
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            return container
        }

        override fun getCount(): Int {
            return 3
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> "Today"
                1 -> "Tommorow"
                2 -> "Day after"
                else -> "Way oba yanda"
            }
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }

    }
}
