package com.wolfpack.rendezvous

import com.wolfpack.rendezvous.models.Rendezvous


class CreateDialogFragment : BaseRendezvousDialog() {
    override fun onPrimaryBtnClick(rendezvous: Rendezvous) {
        listener.onCreateRendezvous(rendezvous)
        dismiss()
    }
}