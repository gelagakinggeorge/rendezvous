package com.wolfpack.rendezvous.data

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.wolfpack.rendezvous.models.Rendezvous

@Dao
interface RendezvousDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg rendezvous: Rendezvous)

    @get:Query("SELECT * from rendezvous_table")
    val all: LiveData<List<Rendezvous>>
}