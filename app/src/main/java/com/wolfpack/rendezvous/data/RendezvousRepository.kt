package com.wolfpack.rendezvous.data

import android.app.Application
import android.arch.lifecycle.LiveData
import com.wolfpack.rendezvous.models.Rendezvous
import com.wolfpack.rendezvous.utilities.runOnIoThread

class RendezvousRepository(application: Application) {
    private val db : AppDatabase = AppDatabase.getAppDatabase(application)
    private val rendezvousDoa : RendezvousDao = db.rendezvousDao()
    private val allRendezvous : LiveData<List<Rendezvous>> = rendezvousDoa.all

    fun getAllRendezvous() = allRendezvous

    fun createRendezvous(rendezvous: Rendezvous)  {
        runOnIoThread {
            rendezvousDoa.insert(rendezvous)
        }
    }

}