package com.wolfpack.rendezvous

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.text.format.DateFormat
import android.util.Log
import android.view.*
import android.widget.*
import com.wolfpack.rendezvous.models.Rendezvous
import com.wolfpack.rendezvous.utilities.ISO_DATE_PATTERN
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import java.text.SimpleDateFormat
import java.util.*

abstract class BaseRendezvousDialog : DialogFragment(), DatePickerDialog.OnDateSetListener {
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
    }

    internal lateinit var listener: RendezvousDialogListener

    interface RendezvousDialogListener {
        fun onCreateRendezvous(rendezvous : Rendezvous)
        fun onUpdateRendezvous(rendezvous: Rendezvous)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as RendezvousDialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException((context.toString() + " must implement RendezvousDialogListener"))
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        // Inflate the layout to use as dialog or embedded fragment
        val root = inflater.inflate(R.layout.fragment_rendezvous, container, false)
        val dateTextView = root.findViewById(R.id.dateTextView) as TextView
        val timeTextView = root.findViewById(R.id.timeTextView) as TextView
        val nameEditText = root.findViewById(R.id.nameEditText) as EditText
        val primaryBtn = root.findViewById(R.id.primary_action_button) as Button

        val calendar = Calendar.getInstance()

        val datePickerListener = getDatePickerListener(dateTextView, calendar)
        val datePicker = createDatePicker(datePickerListener)

        val timePickerListener = getTimePickerListener(timeTextView, calendar)
        val timePicker = createTimePicker(timePickerListener)

        dateTextView.setOnClickListener { _ -> datePicker.show() }
        timeTextView.setOnClickListener { _ -> timePicker.show()}


        primaryBtn.setOnClickListener {
            val name = nameEditText.text.toString()
            val sdf = SimpleDateFormat(ISO_DATE_PATTERN, Locale.ENGLISH)
            val date = sdf.format(calendar.time)
            val rendezvous = Rendezvous(0, name, "", date)
            onPrimaryBtnClick(rendezvous)
        }

        return root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        return dialog
    }

    private fun createDatePicker(listener: DatePickerDialog.OnDateSetListener): DatePickerDialog {
        val localDate = LocalDate.now()
        return DatePickerDialog(context, listener, localDate.year, localDate.monthValue - 1 , localDate.dayOfMonth)
    }

    private fun getDatePickerListener(dateTextView: TextView, calendar: Calendar) : DatePickerDialog.OnDateSetListener{
        return DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            calendar.set(year, month, dayOfMonth)

            val displayFormat = SimpleDateFormat("EEEE, MMMM dd yyyy", Locale.ENGLISH)
            dateTextView.text = displayFormat.format(calendar.time)
        }
    }

    private fun getTimePickerListener(timeTextView: TextView, calendar: Calendar): TimePickerDialog.OnTimeSetListener {
        return TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH), hourOfDay, minute)

            val displayFormat = SimpleDateFormat("hh:mm aaa", Locale.ENGLISH)
            timeTextView.text = displayFormat.format(calendar.time)
        }
    }

    private fun createTimePicker(listener: TimePickerDialog.OnTimeSetListener): TimePickerDialog {
        val now = LocalDateTime.now()
        return TimePickerDialog(context, listener, now.hour, now.minute, false)
    }

    abstract fun onPrimaryBtnClick(rendezvous: Rendezvous)
}