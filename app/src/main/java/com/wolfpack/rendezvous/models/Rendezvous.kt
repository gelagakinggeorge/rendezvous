package com.wolfpack.rendezvous.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "rendezvous_table")
data class Rendezvous(@PrimaryKey(autoGenerate = true) val uid: Int = 0, var name: String, var time: String, var date: String)