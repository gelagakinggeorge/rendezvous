package com.wolfpack.rendezvous

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.wolfpack.rendezvous.models.Rendezvous
import com.wolfpack.rendezvous.utilities.ISO_DATE_PATTERN
import kotlinx.android.synthetic.main.recyclerview_card_rendezvous.view.*
import java.text.SimpleDateFormat
import java.util.*

class RendezvousAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var data : List<Rendezvous> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return RendezvousViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_card_rendezvous, parent, false))
    }

    override fun onBindViewHolder(vh: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = vh as RendezvousViewHolder
        val rendezvous = data[position]
        viewHolder.titleTextView.text = rendezvous.name
        val sdf = SimpleDateFormat(ISO_DATE_PATTERN, Locale.ENGLISH)
        val date = sdf.parse(rendezvous.date)
        val displayFormat = SimpleDateFormat("EEEE, MMMM d, yyyy 'at' h:mm a", Locale.ENGLISH)
        viewHolder.infoTextView.text = displayFormat.format(date)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun setRendezvous(data: List<Rendezvous>) {
        this.data = data
        notifyDataSetChanged()
    }

    class RendezvousViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleTextView = itemView.title as TextView
        val infoTextView = itemView.info as TextView
    }
}