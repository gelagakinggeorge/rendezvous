package com.wolfpack.rendezvous.utilities

const val ISO_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ddZ"